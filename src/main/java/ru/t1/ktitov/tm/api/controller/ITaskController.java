package ru.t1.ktitov.tm.api.controller;

import ru.t1.ktitov.tm.model.Task;

public interface ITaskController {

    void createTask();

    void clearTasks();

    void showTasks();

    void showTaskById();

    void showTaskByIndex();

    void showTask(Task task);

    void showTaskByProjectId();

    void removeTaskById();

    void removeTaskByIndex();

    void updateTaskById();

    void updateTaskByIndex();

    void startTaskById();

    void startTaskByIndex();

    void completeTaskById();

    void completeTaskByIndex();

    void changeTaskStatusById();

    void changeTaskStatusByIndex();

}
